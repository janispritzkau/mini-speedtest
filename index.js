const crypto = require("crypto")
const WebSocket = require("ws")
const http = require("http")
const fs = require("fs")

const RANDOM_BYTES = crypto.randomBytes(1024 * 1024 * 4)

let htmlData
const updateHtml = () => (htmlData = fs.readFileSync("index.html"), console.log("index.html changed"))
fs.watch("index.html", updateHtml), updateHtml()

const server = http.createServer((req, res) => {
    if (req.url == "/") {
        res.write(htmlData)
    } else {
        res.writeHead(404)
    }
    res.end()
})

const wss = new WebSocket.Server({ server })

wss.on("connection", socket => {
    socket.on("message", data => {
        const buffer = Buffer.from(data)
        switch (buffer[0]) {
            case 0: {
                socket.send(buffer)
                break
            }
            case 1: {
                if (buffer.length != 5) return
                const size = buffer.readUInt32BE(1)
                const offset = (Math.random() * Math.max(0, RANDOM_BYTES.length - size)) | 0
                socket.send(RANDOM_BYTES.slice(offset, offset + size))
                break
            }
            case 2: {
                const data = Buffer.alloc(4)
                data.writeUInt32BE(buffer.length, 0)
                socket.send(data)
                break
            }
        }
    })
})

server.listen(8080)
